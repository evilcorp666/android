package com.gp4gp.slamchat.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Alex Alex on 1/24/2017.
 */

public class PresentedActivity extends Activity{
    private EditText editName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presented_activity);

        editName = (EditText) findViewById(R.id.txtEditName);
    }

    public void onPresented(View view){
        Intent intent = new Intent();
        intent.putExtra("name", editName.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
