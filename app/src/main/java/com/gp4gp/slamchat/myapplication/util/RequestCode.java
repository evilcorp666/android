package com.gp4gp.slamchat.myapplication.util;

/**
 * Created by Alex Alex on 1/24/2017.
 */

public class RequestCode {

    public static final int REQUEST_CODE_PRESENTED = 1;
    public static final int REQUEST_CODE_LANGUAGE = 2;
}
