package com.gp4gp.slamchat.myapplication.util;

/**
 * Created by Alex Alex on 1/27/2017.
 */

public enum Language {

    ENGLISH("English"),
    RUSSIAN("Russian"),
    UKRAINE("Ukranian");

    private String language;

    Language(String language){
        this.language = language;
    }

    public String get(){
        return language;
    }
}


