package com.gp4gp.slamchat.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gp4gp.slamchat.myapplication.util.RequestCode;

public class MainActivity extends Activity{

    private TextView txtName;
    private  TextView txtLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        txtName = (TextView) findViewById(R.id.txtName);
        txtLanguage = (TextView)findViewById(R.id.txtLanguage);
    }

    public void onShow(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.btnPresented:
                intent = new Intent(this, PresentedActivity.class);
                startActivityForResult(intent, RequestCode.REQUEST_CODE_PRESENTED);
                break;
            case R.id.btnChoiceLang:
                intent = new Intent(this, LanguageActivity.class);
                startActivityForResult(intent, RequestCode.REQUEST_CODE_LANGUAGE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case RequestCode.REQUEST_CODE_PRESENTED:
                    String name = data.getStringExtra("name");
                    txtName.setText("Nice to meet you  " + name);
                    break;

                case RequestCode.REQUEST_CODE_LANGUAGE:
                    String lang = data.getStringExtra("language");
                    txtLanguage.setText("Your lang is: " + lang);
                    break;
            }
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void showAlert(View view){
        AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this);

        builder.setIcon(R.drawable.ic_stat_name)
                .setTitle("Alarm!!!!!")
                .setMessage("This is message")
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You are not agree", Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
