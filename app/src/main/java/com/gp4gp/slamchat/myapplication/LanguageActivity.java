package com.gp4gp.slamchat.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.gp4gp.slamchat.myapplication.util.Language;

/**
 * Created by Alex Alex on 1/24/2017.
 */

public class LanguageActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_layout);
    }

    public void onSelectLanguage(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.btnEng:
                intent = new Intent();
                intent.putExtra("language", Language.ENGLISH.get());
                setResult(RESULT_OK, intent);
                finish();
            case R.id.btnRus:
                intent = new Intent();
                intent.putExtra("language", Language.RUSSIAN.get());
                setResult(RESULT_OK, intent);
                finish();
            case R.id.btnUkr:
                intent = new Intent();
                intent.putExtra("language", Language.UKRAINE.get());
                setResult(RESULT_OK, intent);
                finish();
        }
    }
}
